#!/bin/bash
export NO_AT_BRIDGE=1

if [[ $(nvidia-settings -q '[gpu:0]/DigitalVibrance[DFP-2]' | grep 'Attribute.*1023\.') ]]
then
	nvidia-settings -a '[gpu:0]/DigitalVibrance[DFP-2]=0' > /dev/null
	echo "Vibrance Disabled"
else
	nvidia-settings -a '[gpu:0]/DigitalVibrance[DFP-2]=1023' > /dev/null
	echo "Vibrance Enabled"
fi
